//
//  ResetPasswordPresenter.swift
//  challenge-capptan
//
//  Created by Arthur Jatoba on 28/09/18.
//  Copyright © 2018 Arthur Jatoba. All rights reserved.
//

import Foundation
import Firebase
import GSMessages


protocol ResetPasswordView {
    func showMessage(msg: String, type: GSMessageType)
    func showLoader()
    func hideLoader()
    func dismissToLogin()
}

protocol ResetPasswordViewPresenter {
    init(view: ResetPasswordView)
    func handleResetPassword(email: String)
}

class ResetPasswordPresenter: ResetPasswordViewPresenter {
    
    var view: ResetPasswordView?
    
    required init(view: ResetPasswordView) {
        self.view = view
    }
    
    func handleResetPassword(email: String) {
        
        view?.showLoader()
        
        Auth.auth().sendPasswordReset(withEmail: email) { (err) in
            if let err = err {
                self.view?.showMessage(msg: err.localizedDescription, type: .error)
                self.view?.hideLoader()
                return
            }
            
            
            self.view?.hideLoader()
            self.view?.dismissToLogin()
        }
    }
    
    
    
    
}
