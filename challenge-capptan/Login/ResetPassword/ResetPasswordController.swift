//
//  ResetPasswordController.swift
//  challenge-capptan
//
//  Created by Arthur Jatoba on 27/09/18.
//  Copyright © 2018 Arthur Jatoba. All rights reserved.
//

import UIKit
import Firebase
import GSMessages

class ResetPasswordController: UIViewController, NVActivityIndicatorViewable {
    
    lazy var presenter: ResetPasswordPresenter = {
        let p = ResetPasswordPresenter(view: self)
        return p
    }()
    
    let logoContainerView: UIView = {
        let view = UIView()
        let logoImageView = UIImageView(image: #imageLiteral(resourceName: "horizontallogocapptan"))
        logoImageView.contentMode = .scaleAspectFill
        
        view.backgroundColor = #colorLiteral(red: 0.8588235294, green: 0.5803921569, blue: 0.01568627451, alpha: 1)
        
        view.addSubview(logoImageView)
        logoImageView.anchor(size: .init(width: 200, height: 50))
        logoImageView.anchorCenterX(anchorX: view.centerXAnchor)
        logoImageView.anchorCenterY(anchorY: view.centerYAnchor)
        
        return view
    }()
    
    let emailTextField: UITextField = {
        let tf = UITextField()
        tf.placeholder = "Email"
        tf.keyboardType = .emailAddress
        tf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        tf.borderStyle = .roundedRect
        tf.font = UIFont.systemFont(ofSize: 14)
        
        tf.addTarget(self, action: #selector(handleTextInputChange), for: .editingChanged)
        return tf
    }()
    
    @objc func handleTextInputChange() {
        let isFormValid = emailTextField.text?.count ?? 0 > 0
        
        if isFormValid {
            resetPasswordButton.isEnabled = true
            resetPasswordButton.backgroundColor = #colorLiteral(red: 0.8588235294, green: 0.5803921569, blue: 0.01568627451, alpha: 1)
        } else {
            resetPasswordButton.isEnabled = false
            resetPasswordButton.backgroundColor = #colorLiteral(red: 0.8588235294, green: 0.5803921569, blue: 0.01568627451, alpha: 0.5543129281)
        }
        
    }
    
    let resetPasswordButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Recuperar Senha", for: .normal)
        button.backgroundColor = #colorLiteral(red: 0.8588235294, green: 0.5803921569, blue: 0.01568627451, alpha: 0.5543129281)
        button.layer.cornerRadius = 5
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.setTitleColor(.white, for: .normal)
        button.addTarget(self, action: #selector(handleResetPassword), for: .touchUpInside)
        button.isEnabled = false
        return button
    }()
    
    @objc func handleResetPassword() {
        
        guard let email = emailTextField.text else { return }
        presenter.handleResetPassword(email: email)
        
    }
    
    let backButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Voltar", for: .normal)
        button.setTitleColor(#colorLiteral(red: 0.2117647059, green: 0.2117647059, blue: 0.2196078431, alpha: 1), for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.addTarget(self, action: #selector(handleGoBack), for: .touchUpInside)
        return button
    }()
    
    @objc func handleGoBack() {
        navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        hideKeyboardWhenTappedAround()
        
        view.addSubview(logoContainerView)
        logoContainerView.anchor(top: view.topAnchor, leading: view.leadingAnchor, trailing: view.trailingAnchor, size: .init(width: 0, height: 150))
        
        setupInputFields()
        
        view.addSubview(backButton)
        backButton.anchor(leading: view.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, trailing: view.trailingAnchor, size: .init(width: 0, height: 50))
    }
    
    private func setupInputFields() {
        
        let stackView = UIStackView(arrangedSubviews: [emailTextField, resetPasswordButton])
        stackView.distribution = .fillEqually
        stackView.spacing = 10
        stackView.axis = .vertical
        
        view.addSubview(stackView)
        stackView.anchorCenterY(anchorY: view.centerYAnchor)
        stackView.anchor(top: nil, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 40, left: 40, bottom: 0, right: 40), size: .init(width: 0, height: 90))

    }
}


extension ResetPasswordController: ResetPasswordView {
    
    func showMessage(msg: String, type: GSMessageType) {
        self.showMessage(msg, type: type, options: [.position(.bottom), .autoHideDelay(5.0), .textNumberOfLines(0),])
    }
    
    func showLoader() {
        startAnimating()
    }
    
    func hideLoader() {
        stopAnimating()
    }
    
    func dismissToLogin() {
        if let navController = self.navigationController, navController.viewControllers.count >= 2 {
            let viewController = navController.viewControllers[navController.viewControllers.count - 2] as? LoginController
            viewController?.showMessage(msg: "Email enviado para recuperação de senha", type: .success)
            print("oi")
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    
}

