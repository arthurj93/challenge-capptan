//
//  SignUpPresenter.swift
//  challenge-capptan
//
//  Created by Arthur Jatoba on 28/09/18.
//  Copyright © 2018 Arthur Jatoba. All rights reserved.
//

import Foundation
import Firebase
import GSMessages


protocol SignUpView: class {
    func showMessage(msg: String, type: GSMessageType)
    func showLoader()
    func hideLoader()
    func dismissToHome()
}

protocol SignUpViewPresenter {
    init(view: SignUpView)
    func handleSignUp(email: String, password: String, name: String)
}


class SignUpPresenter: SignUpViewPresenter {
    
    var view: SignUpView?
    
    required init(view: SignUpView) {
        self.view = view
    }
    

    func handleSignUp(email: String, password: String, name: String) {
        
        view?.showLoader()
        
        Auth.auth().createUser(withEmail: email, password: password) { (response, error) in
            
            if let err = error {
                self.view?.showMessage(msg: err.localizedDescription, type: .error )
                self.view?.hideLoader()

                return
            }
            
            guard let user = response?.user else { return }
            
            print("Sucess created user:", user.uid)
            
            let dictionaryValues = ["name": name]
            let values = [user.uid: dictionaryValues]
            
            Database.database().reference().child("users").updateChildValues(values, withCompletionBlock: { (err, ref) in
                if let err = err {
                    print("Failed to save user info into db:", err)
                    self.view?.hideLoader()
                    return
                }
                
                print("Successfully saved user info to db ")
                self.view?.dismissToHome()

            })
        }
    }
    
}
