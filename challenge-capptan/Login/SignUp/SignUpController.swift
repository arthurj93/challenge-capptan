//
//  SignUpController.swift
//  challenge-capptan
//
//  Created by Arthur Jatoba on 26/09/18.
//  Copyright © 2018 Arthur Jatoba. All rights reserved.
//

import UIKit
import GSMessages

class SignUpController: UIViewController, NVActivityIndicatorViewable {
    
    lazy var presenter: SignUpPresenter = {
        let p = SignUpPresenter(view: self)
        return p
    }()
    
    let logoContainerView: UIView = {
        let view = UIView()
        let logoImageView = UIImageView(image: #imageLiteral(resourceName: "horizontallogocapptan"))
        logoImageView.contentMode = .scaleAspectFill
        
        view.backgroundColor = #colorLiteral(red: 0.8588235294, green: 0.5803921569, blue: 0.01568627451, alpha: 1)
        
        view.addSubview(logoImageView)
        logoImageView.anchor(size: .init(width: 200, height: 50))
        logoImageView.anchorCenterX(anchorX: view.centerXAnchor)
        logoImageView.anchorCenterY(anchorY: view.centerYAnchor)
        
        return view
    }()
    
    let emailTextField: UITextField = {
        let tf = UITextField()
        tf.placeholder = "Email"
        tf.keyboardType = .emailAddress
        tf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        tf.borderStyle = .roundedRect
        tf.font = UIFont.systemFont(ofSize: 14)
        
        tf.addTarget(self, action: #selector(handleTextInputChange), for: .editingChanged)
        return tf
    }()
    
    let passwordTextField: UITextField = {
        let tf = UITextField()
        tf.placeholder = "Senha"
        tf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        tf.borderStyle = .roundedRect
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.autocorrectionType = .no
        if #available(iOS 12.0, *) {
            tf.textContentType = UITextContentType.oneTimeCode
        } else {
            tf.textContentType = UITextContentType.nickname
        }
        //bug ios 12 keeping showing icloud key error strong password
        tf.isSecureTextEntry = true
        tf.addTarget(self, action: #selector(handleTextInputChange), for: .editingChanged)
        return tf
    }()
    
    let nameTextField: UITextField = {
        let tf = UITextField()
        tf.placeholder = "Nome"
        tf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        tf.borderStyle = .roundedRect
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.addTarget(self, action: #selector(handleTextInputChange), for: .editingChanged)
        return tf
    }()
    
    @objc func handleTextInputChange() {
        let isFormValid = emailTextField.text?.count ?? 0 > 0 && nameTextField.text?.count ?? 0 > 0 && passwordTextField.text?.count ?? 0 > 0
        
        if isFormValid {
            signUpButton.isEnabled = true
            signUpButton.backgroundColor = #colorLiteral(red: 0.8588235294, green: 0.5803921569, blue: 0.01568627451, alpha: 1)
        } else {
            signUpButton.isEnabled = false
            signUpButton.backgroundColor = #colorLiteral(red: 0.8588235294, green: 0.5803921569, blue: 0.01568627451, alpha: 0.5543129281)
        }
        
    }
    
    let signUpButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Sign Up", for: .normal)
        button.backgroundColor = #colorLiteral(red: 0.8588235294, green: 0.5803921569, blue: 0.01568627451, alpha: 0.5543129281)
        button.layer.cornerRadius = 5
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.setTitleColor(.white, for: .normal)
        button.addTarget(self, action: #selector(handleSignUp), for: .touchUpInside)
        button.isEnabled = false
        return button
    }()
    
    @objc func handleSignUp() {
        
        guard let email = emailTextField.text, email.count > 0  else { return }
        guard let name = nameTextField.text, name.count > 0 else { return }
        guard let password = passwordTextField.text, password.count > 0 else { return }
        presenter.handleSignUp(email: email, password: password, name: name)
    }
    
    let backButton: UIButton = {
        let button = UIButton(type: .system)
        
        let attributedTitle = NSMutableAttributedString(string: "Já tem uma conta? ", attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        attributedTitle.append(NSAttributedString(string: "Entrar", attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 14), NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.8588235294, green: 0.5803921569, blue: 0.01568627451, alpha: 1)]))
        
        button.setAttributedTitle(attributedTitle, for: .normal)
        button.addTarget(self, action: #selector(handleGoBack), for: .touchUpInside)
        return button
    }()
    
    @objc func handleGoBack() {
        navigationController?.popViewController(animated: true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        view.addSubview(logoContainerView)
        logoContainerView.anchor(top: view.topAnchor, leading: view.leadingAnchor, trailing: view.trailingAnchor, size: .init(width: 0, height: 150))
        
        setupInputFields()
        view.addSubview(backButton)
        backButton.anchor(leading: view.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, trailing: view.trailingAnchor, size: .init(width: 0, height: 50))
    }
    
    
    private func setupInputFields() {
        
        let stackView = UIStackView(arrangedSubviews: [emailTextField, nameTextField, passwordTextField, signUpButton])
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        stackView.spacing = 10
        
        view.addSubview(stackView)
        stackView.anchorCenterY(anchorY: view.centerYAnchor)
        stackView.anchor(top: nil, leading: view.leadingAnchor, trailing: view.trailingAnchor, padding: .init(top: 20, left: 40, bottom: 0, right: 40), size: .init(width: 0, height: 200))
    }


}

extension SignUpController: SignUpView {
    
    func showMessage(msg: String, type: GSMessageType ) {
        self.showMessage(msg, type: type, options: [.position(.bottom), .autoHideDelay(5.0), .textNumberOfLines(0),])
    }
    
    func showLoader() {
        startAnimating()
    }
    
    func hideLoader() {
        stopAnimating()
    }
    
    func dismissToHome() {
        guard let mainTabBarController = UIApplication.shared.keyWindow?.rootViewController as? MainTabBarController else { return }
        mainTabBarController.setupViewControllers()
        self.dismiss(animated: true, completion: {
            mainTabBarController.showMessage("Usuario criado com sucesso", type: .success, options: [.position(.bottom), .autoHideDelay(5.0), .textNumberOfLines(0),])
        })
    }
    
    
}
