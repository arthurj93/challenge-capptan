//
//  LoginPresenter.swift
//  challenge-capptan
//
//  Created by Arthur Jatoba on 26/09/18.
//  Copyright © 2018 Arthur Jatoba. All rights reserved.
//


import Firebase
import GSMessages

protocol LoginView: class  {
    func showMessage(msg: String, type: GSMessageType)
    func showLoader()
    func hideLoader()
    func dismissView()
}

protocol LoginViewPresenter {
    init(view: LoginView)
    func handleLogin(email: String, password:String)
}

class LoginPresenter: LoginViewPresenter {
    
    var view: LoginView?
    
    required init(view: LoginView) {
        self.view = view
    }
    
    func handleLogin(email: String, password: String) {
        
        view?.showLoader()
        
        Auth.auth().signIn(withEmail: email, password: password) { (result, err) in
            if let err = err {
                print("Failed to sign in with email: ", err)
                self.view?.hideLoader()
                self.view?.showMessage(msg: err.localizedDescription, type: .error)
                return
            }
            
            print("Successfully logged back in with user", result?.user.uid ?? "")
            guard let mainTabBarController = UIApplication.shared.keyWindow?.rootViewController as? MainTabBarController else { return }
            mainTabBarController.setupViewControllers()
            self.view?.dismissView()
            
        }
    }
}
