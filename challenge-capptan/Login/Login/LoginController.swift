//
//  LoginController.swift
//  challenge-capptan
//
//  Created by Arthur Jatoba on 26/09/18.
//  Copyright © 2018 Arthur Jatoba. All rights reserved.
//

import UIKit
import Firebase
import GSMessages


class LoginController: UIViewController, NVActivityIndicatorViewable {
    
    lazy var presenter: LoginPresenter = {
        var p = LoginPresenter(view: self)
        return p
    }()
    
    
    let logoContainerView: UIView = {
        let view = UIView()
        let logoImageView = UIImageView(image: #imageLiteral(resourceName: "horizontallogocapptan"))
        logoImageView.contentMode = .scaleAspectFill
        
        view.backgroundColor = #colorLiteral(red: 0.8588235294, green: 0.5803921569, blue: 0.01568627451, alpha: 1)
        
        view.addSubview(logoImageView)
        logoImageView.anchor(size: .init(width: 200, height: 50))
        logoImageView.anchorCenterX(anchorX: view.centerXAnchor)
        logoImageView.anchorCenterY(anchorY: view.centerYAnchor)
        
        return view
    }()
    
    let emailTextField: UITextField = {
        let tf = UITextField()
        tf.placeholder = "Email"
        tf.keyboardType = .emailAddress
        tf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        tf.borderStyle = .roundedRect
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.addTarget(self, action: #selector(handleTextInputChange), for: .editingChanged)
        return tf
    }()
    
    let passwordTextField: UITextField = {
        let tf = UITextField()
        tf.placeholder = "Senha"
        tf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        tf.borderStyle = .roundedRect
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.textContentType = UITextContentType(rawValue: "")
        tf.autocorrectionType = .no
        tf.isSecureTextEntry = true
        tf.addTarget(self, action: #selector(handleTextInputChange), for: .editingChanged)
        return tf
    }()
    
    @objc func handleTextInputChange() {
        let isFormValid = emailTextField.text?.count ?? 0 > 0 && passwordTextField.text?.count ?? 0 > 0
        
        if isFormValid {
            loginUpButton.isEnabled = true
            loginUpButton.backgroundColor = #colorLiteral(red: 0.8588235294, green: 0.5803921569, blue: 0.01568627451, alpha: 1)
        } else {
            loginUpButton.isEnabled = false
            loginUpButton.backgroundColor = #colorLiteral(red: 0.8588235294, green: 0.5803921569, blue: 0.01568627451, alpha: 0.5543129281)
        }
        
    }
    
    let loginUpButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Login", for: .normal)
        button.backgroundColor = #colorLiteral(red: 0.8588235294, green: 0.5803921569, blue: 0.01568627451, alpha: 0.5543129281)
        button.layer.cornerRadius = 5
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.setTitleColor(.white, for: .normal)
        button.addTarget(self, action: #selector(handleLogIn), for: .touchUpInside)
        button.isEnabled = false
        return button
    }()
    
    @objc func handleLogIn() {
        guard let email = emailTextField.text else { return }
        guard let password = passwordTextField.text else { return }

        presenter.handleLogin(email: email, password: password)
        view.endEditing(true)
    }
    
    let resetPasswordButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Recuperar Senha", for: .normal)
        button.setTitleColor(#colorLiteral(red: 0.2117647059, green: 0.2117647059, blue: 0.2196078431, alpha: 1), for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.addTarget(self, action: #selector(handleReset), for: .touchUpInside)
        return button
    }()
    
    @objc func handleReset() {
        let resetPasswordController = ResetPasswordController()
        navigationController?.pushViewController(resetPasswordController, animated: true)
    }
    
    let signUpButton: UIButton = {
        let button = UIButton(type: .system)
        
        let attributedTitle = NSMutableAttributedString(string: "Não tem uma conta? ", attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        attributedTitle.append(NSAttributedString(string: "Cadastre-se", attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 14), NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.8588235294, green: 0.5803921569, blue: 0.01568627451, alpha: 1)]))
        
        button.setAttributedTitle(attributedTitle, for: .normal)
        button.addTarget(self, action: #selector(handleShowSignUp), for: .touchUpInside)
        return button
    }()
    
    @objc func handleShowSignUp() {
        let signUpController = SignUpController()
        navigationController?.pushViewController(signUpController, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        navigationController?.isNavigationBarHidden = true
        
        view.addSubview(logoContainerView)
        logoContainerView.anchor(top: view.topAnchor, leading: view.leadingAnchor, trailing: view.trailingAnchor, size: .init(width: 0, height: 150))
        
        setupInputFields()
        setupTransitionsButtons()
        hideKeyboardWhenTappedAround()

    }
    
    private func setupInputFields() {
        
        let stackView = UIStackView(arrangedSubviews: [emailTextField, passwordTextField, loginUpButton])
        stackView.distribution = .fillEqually
        stackView.spacing = 10
        stackView.axis = .vertical
        
        view.addSubview(stackView)
        stackView.anchor(top: logoContainerView.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 40, left: 40, bottom: 0, right: 40), size: .init(width: 0, height: 140))
    }
    
    private func setupTransitionsButtons() {
        
        view.addSubview(resetPasswordButton)
        resetPasswordButton.anchor(top: loginUpButton.bottomAnchor, leading: nil, bottom: nil, trailing: loginUpButton.trailingAnchor, padding: .init(top: 10, left: 40, bottom: 0, right: 0), size: .init(width: 125, height: 40))
        
        view.addSubview(signUpButton)
        signUpButton.anchor(leading: view.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, trailing: view.trailingAnchor, size: .init(width: 0, height: 50))
    }
    
}

extension LoginController: LoginView {
    
    func dismissView() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func showMessage(msg: String, type: GSMessageType) {
        self.showMessage(msg, type: type, options: [.position(.bottom), .autoHideDelay(5.0), .textNumberOfLines(0),])
    }
    
    func showLoader() {
        startAnimating()
    }
    
    func hideLoader() {
        stopAnimating()
    }
    
}


