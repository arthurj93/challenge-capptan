//
//  CreateAgendaController.swift
//  challenge-capptan
//
//  Created by Arthur Jatoba on 26/09/18.
//  Copyright © 2018 Arthur Jatoba. All rights reserved.
//

import UIKit
import GSMessages

class CreateAgendaController: UIViewController, UITextViewDelegate, NVActivityIndicatorViewable {
    
    lazy var presenter: CreateAgendaPresenter = {
        let p = CreateAgendaPresenter(view: self)
        return p
    }()
    
    let titleTextField: UITextField = {
        let tf = UITextField()
        tf.placeholder = "Título"
        tf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        tf.borderStyle = .roundedRect
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.addTarget(self, action: #selector(handleTextInputChange), for: .editingChanged)
        return tf
    }()
    
    let shortDescriptionTextField: UITextField = {
        let tf = UITextField()
        tf.placeholder = "Breve descrição"
        tf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        tf.borderStyle = .roundedRect
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.addTarget(self, action: #selector(handleTextInputChange), for: .editingChanged)
        return tf
    }()
    
    lazy var authorTextField: UITextField = {
        let tf = UITextField()
        tf.text = presenter.author
        tf.placeholder = "Autor"
        tf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        tf.borderStyle = .roundedRect
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.addTarget(self, action: #selector(handleTextInputChange), for: .editingChanged)
        return tf
    }()
    
    lazy var fullDescriptionTextView: UITextView = {
        let tv = UITextView()
        tv.font = UIFont.systemFont(ofSize: 14)
        tv.text = presenter.descriptionPlaceholder
        tv.backgroundColor = UIColor(white: 0, alpha: 0.03)
        tv.layer.cornerRadius = 5
        tv.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        tv.layer.borderWidth = 1
        tv.textColor = #colorLiteral(red: 0.7411764706, green: 0.7411764706, blue: 0.7647058824, alpha: 1)
        tv.delegate = self
        return tv
    }()
    
    @objc func handleTextInputChange() {
        let isFormValid = titleTextField.text?.count ?? 0 > 0 && shortDescriptionTextField.text?.count ?? 0 > 0 && authorTextField.text?.count ?? 0 > 0 && fullDescriptionTextView.textColor != #colorLiteral(red: 0.7411764706, green: 0.7411764706, blue: 0.7647058824, alpha: 1)
        if isFormValid {
            addButton.isEnabled = true
            addButton.backgroundColor = #colorLiteral(red: 0.8588235294, green: 0.5803921569, blue: 0.01568627451, alpha: 1)
        } else {
            addButton.isEnabled = false
            addButton.backgroundColor = #colorLiteral(red: 0.8588235294, green: 0.5803921569, blue: 0.01568627451, alpha: 0.5543129281)
        }
        
    }
    
    let addButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Adicionar", for: .normal)
        button.backgroundColor = #colorLiteral(red: 0.8588235294, green: 0.5803921569, blue: 0.01568627451, alpha: 0.7004227312)
        button.layer.cornerRadius = 5
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.setTitleColor(.white, for: .normal)
        button.addTarget(self, action: #selector(handleAdd), for: .touchUpInside)
        button.isEnabled = false
        return button
    }()
    
    @objc func handleAdd() {
        
        guard let title = titleTextField.text else { return }
        guard let shortDescription = shortDescriptionTextField.text else { return }
        guard let author = authorTextField.text else { return }
        guard let details = fullDescriptionTextView.text else { return }

        presenter.handleAdd(title: title, shortDescription: shortDescription, author: author, fullDescription: details)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.8588235294, green: 0.5803921569, blue: 0.01568627451, alpha: 1)
        navigationItem.title = "Nova pauta"
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Voltar", style: .plain, target: self, action: #selector(handleDismiss))
        navigationItem.leftBarButtonItem?.tintColor = .black
        setupInputFields()
        hideKeyboardWhenTappedAround()
        
        view.addSubview(addButton)
        addButton.anchor(top: fullDescriptionTextView.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 10, left: 40, bottom: 0, right: 40), size: .init(width: 0, height: 40))
        
    }
    
    @objc func handleDismiss() {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    private func setupInputFields() {
        
        
        view.addSubview(titleTextField)
        view.addSubview(shortDescriptionTextField)
        view.addSubview(authorTextField)
        
        titleTextField.anchor(top: view.safeAreaLayoutGuide.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 20, left: 40, bottom: 0, right: 40), size: .init(width: 0, height: 40))
        
        shortDescriptionTextField.anchor(top: titleTextField.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 10, left: 40, bottom: 0, right: 40), size: .init(width: 0, height: 40))
        
        authorTextField.anchor(top: shortDescriptionTextField.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 10, left: 40, bottom: 0, right: 40), size: .init(width: 0, height: 40))
        
        view.addSubview(fullDescriptionTextView)
        fullDescriptionTextView.anchor(top: authorTextField.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 10, left: 40, bottom: 0, right: 40), size: .init(width: 0, height: 120))
    }
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == fullDescriptionTextView {
            if fullDescriptionTextView.text == presenter.descriptionPlaceholder {
                fullDescriptionTextView.text = nil
                fullDescriptionTextView.textColor = UIColor.black
            }
        }
    }

    func textViewDidChange(_ textView: UITextView) {
        if textView == fullDescriptionTextView {
            if textView.textColor == UIColor.black && !textView.text.trim().isEmpty && titleTextField.text?.count ?? 0 > 0 && shortDescriptionTextField.text?.count ?? 0 > 0 && authorTextField.text?.count ?? 0 > 0 {
                addButton.isEnabled = true
                addButton.backgroundColor = #colorLiteral(red: 0.8588235294, green: 0.5803921569, blue: 0.01568627451, alpha: 1)
            } else {
                addButton.isEnabled = false
                addButton.backgroundColor = #colorLiteral(red: 0.8588235294, green: 0.5803921569, blue: 0.01568627451, alpha: 0.5543129281)
            }
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView == fullDescriptionTextView {
            if fullDescriptionTextView.text.trim().isEmpty {
                fullDescriptionTextView.text = presenter.descriptionPlaceholder
                fullDescriptionTextView.textColor = #colorLiteral(red: 0.7411764706, green: 0.7411764706, blue: 0.7647058824, alpha: 1)
            }
        }
    }
    
}

extension CreateAgendaController: CreateAgendaView {
    
    func showMessage(msg: String, type: GSMessageType ) {
        self.showMessage(msg, type: type, options: [.position(.bottom), .autoHideDelay(5.0), .textNumberOfLines(0),])
    }
    
    func showLoader() {
        startAnimating()
    }
    
    func hideLoader() {
        stopAnimating()
    }
    
    func dismissView() {
        self.dismiss(animated: true, completion: {
            AgendaPresenter.openItemBar = true
            guard let mainTabBarController = UIApplication.shared.keyWindow?.rootViewController as? MainTabBarController else { return }
            mainTabBarController.selectedIndex = 0
            mainTabBarController.stopAnimating()
            mainTabBarController.showMessage("Pauta criada com sucesso!", type: .success, options: [.position(.bottom), .autoHideDelay(5.0), .textNumberOfLines(0),])
            
        })

    }
    
}

