//
//  CreateAgendaPresenter.swift
//  challenge-capptan
//
//  Created by Arthur Jatoba on 28/09/18.
//  Copyright © 2018 Arthur Jatoba. All rights reserved.
//

import Foundation
import Firebase
import GSMessages

protocol CreateAgendaView {
    func showLoader()
    func hideLoader()
    func showMessage(msg: String, type: GSMessageType)
    func dismissView()
}

protocol CreateAgendaViewPresenter {
    init(view: CreateAgendaView)
    func handleAdd(title: String, shortDescription: String, author: String, fullDescription: String)
}

class CreateAgendaPresenter: CreateAgendaViewPresenter {
    
    let descriptionPlaceholder = "Detalhes"
    
    var author = String()
    
    var view: CreateAgendaView?
    required init(view: CreateAgendaView) {
        self.view = view
    }
    
    func handleAdd(title: String, shortDescription: String, author: String, fullDescription: String) {
        
        view?.showLoader()
        let id = NSUUID().uuidString
        let post = Database.database().reference().child("pautas").child(id)
        
        let values = ["id": id,"title": title, "shortDescription": shortDescription, "author": author, "fullDescription": fullDescription, "creationDate": Date().timeIntervalSince1970, "updateDate": Date().timeIntervalSince1970, "status": AgendaStatus.open.rawValue] as [String: Any]
        
        post.updateChildValues(values) { (err, ref) in
            
            if let err = err {
                print("Failed to save agenda: ", err)
                self.view?.hideLoader()
                self.view?.showMessage(msg: err.localizedDescription, type: .error)
                return
            }
            
            print("Successfully save agenda to db")
            self.view?.dismissView()
        }
    }
    
    
    
}
