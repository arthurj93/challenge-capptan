//
//  MainTabBarController.swift
//  challenge-capptan
//
//  Created by Arthur Jatoba on 26/09/18.
//  Copyright © 2018 Arthur Jatoba. All rights reserved.
//

import UIKit
import GSMessages

class MainTabBarController: UITabBarController, UITabBarControllerDelegate, NVActivityIndicatorViewable {
    
    lazy var presenter: MainTabBarViewPresenter = {
        let p = MainTabBarPresenter(view: self)
        return p
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.authLogin()
        setupViewControllers()
        
    }
    
    func setupViewControllers() {
        let unlockedController = templateNavController(unselectedImage: #imageLiteral(resourceName: "unlocked-unselected"), selectedImage: #imageLiteral(resourceName: "unlocked-selected"), rootViewController: AgendaController(collectionViewLayout: UICollectionViewFlowLayout()))
        
        let lockedController = templateNavController(unselectedImage: #imageLiteral(resourceName: "locked-unselected"), selectedImage: #imageLiteral(resourceName: "locked-selected"), rootViewController: AgendaController(collectionViewLayout: UICollectionViewFlowLayout()))
        
        tabBar.tintColor = .black
        
        viewControllers = [unlockedController, lockedController]
        
        //modify tab bar item insets
        
        guard let items = tabBar.items else { return }
        
        for (i ,item) in items.enumerated() {
            item.imageInsets = .init(top: 4, left: 0, bottom: -4, right: 0)
            item.tag = i
        }
        
    }
    
    private func templateNavController(unselectedImage: UIImage, selectedImage: UIImage, rootViewController: UIViewController = UIViewController()) -> UINavigationController {
        let viewController = rootViewController
        let navController = UINavigationController(rootViewController: viewController)
        navController.tabBarItem.image = unselectedImage
        navController.tabBarItem.selectedImage = selectedImage
        
        return navController
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if item.tag == 0 {
            AgendaPresenter.openItemBar = true
        } else {
            AgendaPresenter.openItemBar = false
        }
    }
}

extension MainTabBarController: MainTabBarView {
    
    func presentHome(navController: UINavigationController) {
        self.present(navController, animated: true, completion: nil)
    }
    
    
}
