//
//  MainTabBarPresenter.swift
//  challenge-capptan
//
//  Created by Arthur Jatoba on 28/09/18.
//  Copyright © 2018 Arthur Jatoba. All rights reserved.
//

import Foundation
import Firebase

protocol MainTabBarView : class {
    func presentHome(navController: UINavigationController)
}

protocol MainTabBarViewPresenter {
    init(view: MainTabBarView)
    func authLogin()
}


class MainTabBarPresenter: MainTabBarViewPresenter {
    
    var view: MainTabBarView?
    
    required init(view: MainTabBarView) {
        self.view = view
    }
    
    func authLogin() {
        if Auth.auth().currentUser == nil {
            DispatchQueue.main.async {
                let loginController = LoginController()
                let navController = UINavigationController(rootViewController: loginController)
                self.view?.presentHome(navController: navController)
            }
            return
        }
    }
    
    
}
