//
//  AgendaTitleCell.swift
//  challenge-capptan
//
//  Created by Arthur Jatoba on 26/09/18.
//  Copyright © 2018 Arthur Jatoba. All rights reserved.
//

import UIKit

class AgendaTitleCell: UICollectionViewCell {
    
    static let cellId = "agendaTitleCellId"
    
    var agenda: Agenda? {
        didSet {
            
            guard let title = agenda?.title else { return }
            guard let shortDescription = agenda?.shortDescription else { return }
            titleLabel.text = title
            descriptionLabel.text = shortDescription
        }
    }
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "titulo da pauta"
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.numberOfLines = 0
        label.textAlignment = .center
        label.textColor = .white
        return label
    }()
    
    let descriptionLabel: UILabel = {
        let label = UILabel()
        label.text = "descrição curta"
        label.font = UIFont.systemFont(ofSize: 14)
        label.numberOfLines = 0
        label.textColor = .white
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(titleLabel)
        addSubview(descriptionLabel)
        backgroundColor = #colorLiteral(red: 0.2117647059, green: 0.2117647059, blue: 0.2196078431, alpha: 1)
        
        titleLabel.anchor(top: topAnchor, leading: leadingAnchor, bottom: descriptionLabel.topAnchor, trailing: trailingAnchor, padding: .init(top: 8, left: 8, bottom: 0, right: 8))
        descriptionLabel.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor, padding: .init(top: 8, left: 8, bottom: 8, right: 8))
        
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
