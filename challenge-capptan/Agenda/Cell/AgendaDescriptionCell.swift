//
//  AgendaDescriptionCell.swift
//  challenge-capptan
//
//  Created by Arthur Jatoba on 26/09/18.
//  Copyright © 2018 Arthur Jatoba. All rights reserved.
//

import UIKit

class AgendaDescriptionCell: UICollectionViewCell {
    
    static let cellId = "agendaDescriptionCellId"
    
    var agenda: Agenda? {
        didSet {
            authorLabel.text = "Autor: \(agenda?.author ?? "")"
            descriptionLabel.text = agenda?.fullDescription
            
            if agenda?.status == AgendaStatus.open.rawValue {
                statusButton.setTitle("Fechar", for: .normal)
            } else {
                statusButton.setTitle("Abrir", for: .normal)
            }
        }
    }
    
    let authorLabel: UILabel = {
        let label = UILabel()
        label.text = "nome do author"
        label.font = UIFont.systemFont(ofSize: 14)
        label.numberOfLines = 0
        label.textColor = .white
        return label
    }()
    
    let descriptionLabel: UILabel = {
        let label = UILabel()
        label.text = "descrição curta"
        label.font = UIFont.systemFont(ofSize: 14)
        label.numberOfLines = 0
        label.textColor = .white
        return label
    }()
    
    lazy var statusButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Finalizar", for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.backgroundColor = #colorLiteral(red: 0.2117647059, green: 0.2117647059, blue: 0.2196078431, alpha: 1)
        button.layer.cornerRadius = 5
        button.addTarget(self, action: #selector(handleTapped), for: .touchUpInside)
        return button
    }()
    
    weak var delegate: AgendaDescriptionCellDelegate?
    @objc func handleTapped() {

        self.delegate?.didSelectButton(cell: self)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .gray
        addSubview(authorLabel)
        addSubview(descriptionLabel)
        addSubview(statusButton)
        
        authorLabel.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 8, left: 8, bottom: 0, right: 8))
        descriptionLabel.anchor(top: authorLabel.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 8, left: 8, bottom: 0, right: 8))
        
        statusButton.anchorCenterX(anchorX: centerXAnchor)
        statusButton.anchor(top: descriptionLabel.bottomAnchor, leading: nil, bottom: bottomAnchor, trailing: nil, padding: .init(top: 8, left: 0, bottom: 8, right: 0), size: .init(width: 100, height: 30))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
