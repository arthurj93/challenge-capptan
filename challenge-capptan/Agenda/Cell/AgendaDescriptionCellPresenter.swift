//
//  AgendaDescriptionCellPresenter.swift
//  challenge-capptan
//
//  Created by Arthur Jatoba on 28/09/18.
//  Copyright © 2018 Arthur Jatoba. All rights reserved.
//

import Foundation

protocol AgendaDescriptionCellDelegate: class {
    func didSelectButton(cell: AgendaDescriptionCell)
}
