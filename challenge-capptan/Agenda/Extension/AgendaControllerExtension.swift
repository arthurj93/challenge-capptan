//
//  AgendaControllerExtension.swift
//  challenge-capptan
//
//  Created by Arthur Jatoba on 27/09/18.
//  Copyright © 2018 Arthur Jatoba. All rights reserved.
//

import UIKit

extension AgendaController: UICollectionViewDelegateFlowLayout {
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        if AgendaPresenter.openItemBar {
            return presenter.openAgenda.count
        } else {
            return presenter.closedAgenda.count
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if AgendaPresenter.openItemBar {
            if presenter.openAgenda[section].open {
                return 2
            } else {
                return 1
            }
        } else {
            if presenter.closedAgenda[section].open {
                return 2
            } else {
                return 1
            }
        }
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        return configureCell(isOpen: AgendaPresenter.openItemBar, indexPath: indexPath)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return configureCellSize(isOpen: AgendaPresenter.openItemBar, indexPath: indexPath)
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        addOrRemoveCell(isOpen: AgendaPresenter.openItemBar, indexPath: indexPath)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 0, bottom: 0, right: 0)
    }
    
    // CONGIFURE CELL FUNCTIONS
    //
    //
    //
    
    private func configureCellSize(isOpen: Bool, indexPath: IndexPath) -> CGSize{
        
        var agenda = [Agenda]()
        if isOpen {
            agenda = presenter.openAgenda
        } else {
            agenda = presenter.closedAgenda
        }
        
        if indexPath.item == 0 {
            let title = agenda[indexPath.section].title
            let shortDescription = agenda[indexPath.section].shortDescription
            
            let approxTitle = collectionView.frame.width - 16
            let sizeTitle = CGSize(width: approxTitle, height: 1000)
            let attributesTitle = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 14)]
            let estimatedFrameTitle = NSString(string: title).boundingRect(with: sizeTitle, options: .usesLineFragmentOrigin, attributes: attributesTitle, context: nil)
            
            let approxShortDescription = collectionView.frame.width - 16
            let sizeShortDescription = CGSize(width: approxShortDescription, height: 1000)
            let attributesShortDescription = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14)]
            let estimatedFrameShortDescription = NSString(string: shortDescription).boundingRect(with: sizeShortDescription, options: .usesLineFragmentOrigin, attributes: attributesShortDescription, context: nil)
            
            let sumHeight = estimatedFrameTitle.height + estimatedFrameShortDescription.height + 24 + 2
            return CGSize(width: view.frame.width, height: sumHeight)
            
        } else {
            
            let author = "Autor: \(agenda[indexPath.section].author)"
            let fullDescription = agenda[indexPath.section].fullDescription
            
            let approxAuthor = collectionView.frame.width - 16
            let sizeAuthor = CGSize(width: approxAuthor, height: 1000)
            let attributesAuthor = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14)]
            let estimatedFrameAuthor = NSString(string: author).boundingRect(with: sizeAuthor, options: .usesLineFragmentOrigin, attributes: attributesAuthor, context: nil)
            
            let approxFullDescription = collectionView.frame.width - 16
            let sizeFullDescription = CGSize(width: approxFullDescription, height: 1000)
            let attributesFullDescription = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14)]
            let estimatedFrameFullDescription = NSString(string: fullDescription).boundingRect(with: sizeFullDescription, options: .usesLineFragmentOrigin, attributes: attributesFullDescription, context: nil)
            
            let sumHeight = estimatedFrameAuthor.height + estimatedFrameFullDescription.height + 32 + 2 + 30
            
            return CGSize(width: view.frame.width, height: sumHeight)
            
        }
    }

    private func addOrRemoveCell(isOpen: Bool, indexPath: IndexPath) {
        
        var agenda = [Agenda]()
        
        if isOpen {
            agenda = presenter.openAgenda
        } else {
            agenda = presenter.closedAgenda
        }
        
        if indexPath.item == 0 {
            if agenda[indexPath.section].open {
                
                agenda[indexPath.section].open = false
                let sections = IndexSet.init(integer: indexPath.section)
                
                if isOpen {
                    presenter.openAgenda = agenda
                } else {
                    presenter.closedAgenda = agenda
                }
                
                collectionView.reloadSections(sections)
                
            } else {
                for (index, pauta) in agenda.enumerated() {
                    if pauta.open {
                        
                        agenda[index].open = false
                        let sections = IndexSet.init(integer: index)
                        
                        if isOpen {
                            presenter.openAgenda = agenda
                        } else {
                            presenter.closedAgenda = agenda
                        }
                        
                        collectionView.reloadSections(sections)
                    }
                }
                agenda[indexPath.section].open = true
                let sections = IndexSet.init(integer: indexPath.section)
                let newIndex = IndexPath(item: 1, section: indexPath.section)
                
                if isOpen {
                    presenter.openAgenda = agenda
                } else {
                    presenter.closedAgenda = agenda
                }
                
                collectionView.reloadSections(sections)
                collectionView.scrollToItem(at: newIndex, at: .bottom, animated: true)
            }
        }
    }
    
    private func configureCell(isOpen: Bool, indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.item == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: AgendaTitleCell.cellId, for: indexPath) as! AgendaTitleCell
            
            if isOpen {
                cell.agenda = presenter.openAgenda[indexPath.section]
                return cell
            } else {
                cell.agenda = presenter.closedAgenda[indexPath.section]
                return cell
            }
            
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: AgendaDescriptionCell.cellId, for: indexPath) as! AgendaDescriptionCell
            
            if isOpen {
                cell.agenda = presenter.openAgenda[indexPath.section]
                cell.delegate = self
                return cell
            } else {
                cell.agenda = presenter.closedAgenda[indexPath.section]
                cell.delegate = self
                return cell
            }
        }
    }
    
}

