//
//  AgendaControllerPresenter.swift
//  challenge-capptan
//
//  Created by Arthur Jatoba on 28/09/18.
//  Copyright © 2018 Arthur Jatoba. All rights reserved.
//

import Foundation
import Firebase
import GSMessages

protocol AgendaView {
    func showLoader()
    func hideLoader()
    func reloadCV()
    func presentView(view: UIViewController)
    func showMessage(msg: String, type: GSMessageType)
}

protocol AgendaViewPresenter {
    init(view: AgendaView)
    
    func fetchOrderedAgenda()
    func handleLogOut()
    func fetchUser()
    func didSelectButton(collectionView: UICollectionView, cell: AgendaDescriptionCell)
}


class AgendaPresenter: AgendaViewPresenter {
    
    static var openItemBar = true
    
    var view: AgendaView?
    var openAgenda = [Agenda]()
    var closedAgenda = [Agenda]()
    var author = String()
    
    required init(view: AgendaView) {
        self.view = view
    }
    
    func fetchOrderedAgenda() {
        
        let ref = Database.database().reference().child("pautas")
        
        ref.queryOrdered(byChild: "creationDate").observe(.childChanged, with: { (snapshot) in
            
            guard let dictionary = snapshot.value as? [String: Any] else { return }
            let agenda = Agenda(dictionary: dictionary)
            if agenda.status == AgendaStatus.open.rawValue {
                self.openAgenda.append(agenda)
                self.openAgenda.sort(by: { (p1, p2) -> Bool in
                    return p1.updateDate.compare(p2.updateDate) == .orderedDescending
                })
            } else {
                self.closedAgenda.append(agenda)
                self.closedAgenda.sort(by: { (p1, p2) -> Bool in
                    return p1.updateDate.compare(p2.updateDate) == .orderedDescending
                })
            }
            
            self.view?.reloadCV()
            
        }) { (err) in
            print("Faield to fetch ordered posts", err)
        }
        
        ref.queryOrdered(byChild: "creationDate").observe(.childAdded, with: { (snapshot) in
            guard let dictionary = snapshot.value as? [String: Any] else { return }
            let agenda = Agenda(dictionary: dictionary)
            if agenda.status == AgendaStatus.open.rawValue {
                self.openAgenda.append(agenda)
                self.openAgenda.sort(by: { (p1, p2) -> Bool in
                    return p1.updateDate.compare(p2.updateDate) == .orderedDescending
                })
            } else {
                self.closedAgenda.append(agenda)
                self.closedAgenda.sort(by: { (p1, p2) -> Bool in
                    return p1.updateDate.compare(p2.updateDate) == .orderedDescending
                })
            }
            self.view?.reloadCV()
        }) { (err) in
            print("Failed to fetch ordered posts: ", err)
        }
    }
    
    func handleLogOut() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: "Log Out", style: .destructive, handler: { (_) in
            do {
                try Auth.auth().signOut()
                
                let loginController = LoginController()
                let navController = UINavigationController(rootViewController: loginController)
                self.view?.presentView(view: navController)
            } catch let signOutErr {
                print("Failed to sign out: ", signOutErr)
            }
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        view?.presentView(view: alertController)
    }
    
    func fetchUser() {
        
        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        Database.database().reference().child("users").child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
            guard let dictionary = snapshot.value as? [String: Any] else { return }
            self.author = dictionary["name"] as? String ?? ""
            
        }) { (err) in
            print("Failed to fetch user: ", err)
        }
    }
    
    func didSelectButton(collectionView: UICollectionView ,cell: AgendaDescriptionCell) {

        view?.showLoader()
        
        if let indexPath = collectionView.indexPath(for: cell) {

            let agenda: Agenda?
            var message = ""
            if AgendaPresenter.openItemBar {
                message = "Pauta fechada"
                agenda = self.openAgenda[indexPath.section]
                agenda?.status = AgendaStatus.closed.rawValue
            } else {
                message = "Pauta aberta"
                agenda = self.closedAgenda[indexPath.section]
                agenda?.status = AgendaStatus.open.rawValue
            }
            guard let agendaUnwrapped = agenda else { return }

            let dateNow = agendaUnwrapped.updateDate.timeIntervalSince1970

            let post = Database.database().reference().child("pautas").child(agendaUnwrapped.id)

            let values = ["id": agendaUnwrapped.id,"title": agendaUnwrapped.title, "shortDescription": agendaUnwrapped.shortDescription, "author": agendaUnwrapped.author, "fullDescription": agendaUnwrapped.fullDescription, "creationDate": agendaUnwrapped.creationDate.timeIntervalSince1970, "updateDate": dateNow, "status": agendaUnwrapped.status] as [String: Any]
            post.updateChildValues(values) { (err, ref) in
                if let err = err {
                    print("Failed to save agenda: ", err)
                    self.view?.hideLoader()
                    self.view?.showMessage(msg: err.localizedDescription, type: .error)
                    return
                }

                print("Successfully save agenda to db")
                self.view?.hideLoader()
                self.view?.showMessage(msg: message, type: .warning)
            }

            if AgendaPresenter.openItemBar {
                self.openAgenda.remove(at: indexPath.section)
            } else {
                self.closedAgenda.remove(at: indexPath.section)
            }
            view?.reloadCV()

        }
    }
}


