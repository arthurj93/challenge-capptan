//
//  AgendaController.swift
//  challenge-capptan
//
//  Created by Arthur Jatoba on 26/09/18.
//  Copyright © 2018 Arthur Jatoba. All rights reserved.
//

import UIKit
import GSMessages

class AgendaController: UICollectionViewController, NVActivityIndicatorViewable {
    
//    static var openItemBar = true
    
    lazy var presenter: AgendaPresenter = {
        let p = AgendaPresenter(view: self)
        return p
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.backgroundColor = .white
        collectionView.register(AgendaTitleCell.self, forCellWithReuseIdentifier: AgendaTitleCell.cellId)
        collectionView.register(AgendaDescriptionCell.self, forCellWithReuseIdentifier: AgendaDescriptionCell.cellId)
        setupLogOutButton()
        
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.8588235294, green: 0.5803921569, blue: 0.01568627451, alpha: 1)
        if AgendaPresenter.openItemBar {
            self.navigationItem.title = "Pautas abertas"
        } else {
            self.navigationItem.title = "Pautas fechadas"
        }
        
        fetchOrderedAgenda()
        stopAnimating()
        fetchUser()
    }
    
    func fetchOrderedAgenda() {
        presenter.fetchOrderedAgenda()
        
    }
    
    private func setupLogOutButton() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "gear").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(handleLogOut))
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "addicon").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(handleCreateAgenda))
    }
    
    @objc func handleCreateAgenda() {
        let createAgendaController = CreateAgendaController()
        createAgendaController.presenter.author = presenter.author
        let navController = UINavigationController(rootViewController: createAgendaController)
        self.present(navController, animated: true, completion: nil)
    }
 
    @objc func handleLogOut() {
        presenter.handleLogOut()
    }
    
    private func fetchUser() {
        
        presenter.fetchUser()
        
    }

}

extension AgendaController: AgendaView {
    func showMessage(msg: String, type: GSMessageType) {
        self.showMessage(msg, type: type, options: [.position(.bottom), .autoHideDelay(5.0), .textNumberOfLines(0),])
    }
    
    func showLoader() {
        startAnimating()
    }
    
    func hideLoader() {
        stopAnimating()
    }
    
    func reloadCV() {
        self.collectionView.reloadData()
    }
    
    func presentView(view: UIViewController) {
        present(view, animated: true, completion: nil)
    }
}



extension AgendaController: AgendaDescriptionCellDelegate {
    func didSelectButton(cell: AgendaDescriptionCell) {
        presenter.didSelectButton(collectionView: collectionView, cell: cell)
    }
}
