//
//  StringExtension.swift
//  study-ios-instagram
//
//  Created by Arthur Jatoba on 19/09/18.
//  Copyright © 2018 Arthur Jatoba. All rights reserved.
//

import UIKit
extension String {
    
    func trim() -> String{
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
}
