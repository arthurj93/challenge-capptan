//
//  UINavigationController.swift
//  study-ios-instagram
//
//  Created by Arthur Jatoba on 21/09/18.
//  Copyright © 2018 Arthur Jatoba. All rights reserved.
//

import UIKit

extension UINavigationController {
    open override var preferredStatusBarStyle: UIStatusBarStyle {
        return topViewController?.preferredStatusBarStyle ?? .lightContent
    }
}
