//
//  Agenda.swift
//  challenge-capptan
//
//  Created by Arthur Jatoba on 27/09/18.
//  Copyright © 2018 Arthur Jatoba. All rights reserved.
//

import UIKit

struct Agenda {
    let id: String
    let title: String
    let shortDescription: String
    let fullDescription: String
    var status: String
    let author: String
    var creationDate: Date
    var updateDate: Date
    var open: Bool = false
    
    init(dictionary: [String: Any]) {
        self.id = dictionary["id"] as? String ?? ""
        self.title = dictionary["title"] as? String ?? ""
        self.shortDescription = dictionary["shortDescription"] as? String ?? ""
        self.fullDescription = dictionary["fullDescription"] as? String ?? ""
        self.status = dictionary["status"] as? String ?? ""
        self.author = dictionary["author"] as? String ?? ""
        self.creationDate = dictionary["creationDate"] as? Date ?? Date()
        self.updateDate = dictionary["updateDate"] as? Date ?? Date()
        self.open = false
    }
}


enum AgendaStatus: String {
    case open = "Aberta"
    case closed = "Fechada"
}
